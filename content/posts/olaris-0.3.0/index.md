---
title: "Olaris 0.3: Outpost"
featuredImage: "posts/olaris-0.3.0/banner.png"
date: 2019-12-04T12:52:25+01:00
description: "Olaris 0.3.0 is Olaris‘ third feature release. This release features Chromecast support and manual retagging of mistagged movies and TV shows."
categories: ["Release", "Updates"]
draft: false
---

It's fall again, the leaves are turning brown, the days are getting shorter and the first cold days send us looking for our warm coat in that long-forgotten corner of our wardrobe. 'Tis the season of new seasons, when our favorite TV shows return to bring us new weekly episodes. So it's also time for a new release of Olaris, it's been a while! We are proud to release version 0.3.0 of the Olaris Media Server, now with Chromecast support, the ability to correct mistagged shows and movies and many more smaller improvements, visible and under the hood, including improved rclone support!

### Cast to your TV with Chromecast

Chromecast support is a big one. It brings your TV shows and movies to the big screen. When you click the little Chromecast button that appears when Olaris finds a Chromecast device on your network, your computer or phone becomes a remote for your television: whatever you play gets instantly sent to your television All movies and TV shows playable by Olaris are compatible - if required, they are transcoded on the fly to a format that Chromecast can. Oh, and did we mention it comes with subtitle support?

Please not that Chromecast playback is only supported over SSL, if Olaris is not hosted on a server with a valid certificate the icon won't show up. This is a restriction imposed by Google.

![Screenshot of Chromecast functionality](chromecast_screenshot.png)

### Retag mismatched media

Olaris automatically recognizes your media and makes it come alive in a library view with descriptions and full-size posters. However, it can sometimes get things wrong, for example if movies have very similar names. Olaris now allows you to correct these mismatched associations. If Olaris couldn't guess the show or movie by inspecting the file, you can now manually tag unidentified files.

![Screenshot of retagging functionality](retagging_screenshot.png)

### Improved remote playback support

Olaris supports playing media directly from your Google Drive, Dropbox or a gazillion other services using rclone. No need to download a movie before playing it - just set up your account in rclone, add it in the Olaris interface and play your media like it was stored on your own computer. We first made this feature available for testing in our [0.2 release](https://blog.olaris.tv/posts/olaris-0.2.0-released/) and have continued to make it more stable since then.

### Olaris Rename

If you have files that Olaris has trouble recognizing you can now run our companion application "Olaris Rename" to identify and symlink, hardlink or copy files. In it's most basic form you can run it with just one argument `olaris-rename --filepath=/path/to/folder/or/file` and it should be able to figure things out from there. It accepts a handful of arguments to change the default behaviour though if you don't like the defaults. Olaris Rename is also free and open-source and can be [downloaded on Gitlab.](https://gitlab.com/olaris/olaris-rename/-/releases)

### Many more improvements

In addition to these features, there has also been many small improvements, fixes and thoughts going in to this release, both visible and under the hood.

To name few:

* Tweaks to the media overview pages for a more consistent feel throughout
* A redesigned user configuration page where you can add new users to your Olaris instance.
* Refactorings to keep up with new developments in the React framework we use to build the web interface for Olaris
* Many refactorings under the hood to make our database code easier, enabling more rapid development in the future. Many thanks to [Björn Dahlgren](https://gitlab.com/BjornDahlgren) for his contributions!
* To avoid breaking things in the future, we added more tests to the server codebase.
* We now release versioned Docker images on Docker Hub

### Try it out & get involved!

The easiest way to try out Olaris is to [use our pre-build Docker image](https://hub.docker.com/r/olaristv/olaris-server). If you're on Linux, you can [download our prebuilt binary](https://storage.googleapis.com/bysh-chef-files/olaris-release/olaris-linux-amd64-v0.3.2.zip) or just [run from source directly](https://gitlab.com/olaris/olaris-server). For more documentation on how to run Olaris, see our [README](https://gitlab.com/olaris/olaris-server/blob/develop/README.md).

If you are using an Appbox from [Bytesized hosting](http://bytesized-hosting.com/) you can install Olaris from the App center or, if it's already installed, restart for the new version to boot up.

We hope that you enjoy these new features. As always, please let us know about your experience. Hit us up on [Discord](https://discord.gg/eKHsJV3) or report bugs on our [bugtracker](https://gitlab.com/olaris/olaris-server/issues).

If you'd like to dig deeper, we encourage all kinds of contributions, whether you'd like to write code, contribute UX ideas and concepts for new features or help shape the future of Olaris by chiming in on Discord and on the bug tracker. We want Olaris to be a community-driven project and hope that you share our vision!
