---
title: "Olaris 0.2: Nephelae"
featuredImage: "https://i.imgur.com/gPo1z6H.jpg"
date: 2019-06-20T11:12:53+02:00
description: "Olaris 0.2.0 is Olaris's second feature release. This release is
all about supporting various 'Cloud' hosting providers so your library no
longer has to live on your local filesystem. Continue reading for more
about the changes in this release."
categories: ["Release", "Updates"]
draft: false
---

It's been a month since Olaris's first release and we have been hard at
work at making various improvements; big and small. Let's start with the
big one first.

We've added support for a dozen or so cloud hosting providers so that
your files can be indexed and streamed directly from an online source.
The following providers are supported with the 0.2.0 release.

* Alibaba Cloud (Aliyun) Object Storage System (OSS)  
* Amazon S3  
* Backblaze B2  
* Box  
* Ceph  
* DigitalOcean Spaces  
* Dreamhost  
* Dropbox  
* Google Cloud Storage  
* Google Drive  
* Hubic  
* Jottacloud  
* IBM COS S3  
* Koofr  
* Memset Memstore  
* Mega  
* Microsoft Azure Blob Storage  
* Microsoft OneDrive  
* Minio  
* Nextcloud  
* OVH  
* OpenDrive  
* Openstack Swift  
* Oracle Cloud Storage  
* ownCloud  
* pCloud  
* put.io  
* QingStor  
* Rackspace Cloud Files  
* rsync.net  
* Scaleway  
* SFTP  
* Wasabi  
* WebDAV  
* Yandex Disk  
* The local filesystem 

Now obviously a month is not enough to build in support for all these
services ourselves, we managed to get this done in such a short time
because we built on top of the shoulders of a giant:
[Rclone](http://rclone.com). For those that are unaware Rclone is a
command line tool to interact with various cloud hosting platforms.
Because Rclone is written in the same language as Olaris we were able to
integrate Rclone as a library inside the Olaris codebase. This means
that we only had to (re-)write our codebase to support indexing and
streaming from an Rclone backend and Rclone took care of all the other details
for us.

To get connected to a cloud hosting provider Olaris 0.2 expects that you already have a working Rclone
configuration on the server where Olaris is installed. There is no way
yet to setup an Rclone remote from within Olaris although we do plan to
do this at a later date. FUSE is not needed for this to work.

We have not been able to test all the providers that should be supported
so please let us know how things work for you, and more specifically if
they don't work. We are also very interested in hearing about the API calls being
produced with the various remote configurations (cache, non-cache).

### Adding a cloud library

To add a cloud library with Rclone first ensure you have a working
Rclone configuration setup.

1. Navigate to your Olaris and press the "+"-sign next to the library
type you want to add.
2. Select "Rclone" in the "Library Type" dialog.
3. A new select box should appear with the remotes Olaris found in your
Rclone config file, select the one you want to add.
4. Lastly enter the path you want to index on this remote.
{{<smallimg src="https://i.imgur.com/62585X6.png" alt="Add an Rclone remote" height="35px">}}

There is no local filesystem involved in this procedure so always make
sure you enter the path from the root of your cloud provider. Do note
that indexing from a cloud provider will be _much_ slower than from a local
filesystem because of the higher latency of the cloud storage. Files from cloud
storage may also take a tad longer to start playback. However, playback should
be smooth and stutter-free as usual.

### Other changes in 0.2.0

Here are some other smaller things that got changed in this Olaris
release.

* Better performance on big (200+ files) libraries thanks to auto-loading
pagination. Initially it would load all content in one big request on
larger libaries of thousands of items this could take up to a minute.
Thanks to our first contributor [LukeWH](http://lukewh.com/) we now have
an optimised way of dealing with these libraries where it will load
content in the back as you scroll towards the end of the current items;
* We made a few small changes to the way filename's are parsed for
content discovery which will result in more files being correctly
identified that were not before;
* Fixed a race condition that could sometimes make Olaris crash during
library scanning;
* Rewrote the merging that happens when a movie is found multiple times
to hopefully reduce duplicate movies showing up in your library;
* Various frontend tweaks for an even cleaner experience;

### Download

Olaris 0.2 can be [downloaded
here](https://storage.googleapis.com/bysh-chef-files/olaris-linux-amd64), please give it a whirl and let us
know what you think.

As always issues can be reported on our [issue
tracker](https://gitlab.com/olaris/olaris-server/issues).
