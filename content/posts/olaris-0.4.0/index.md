---
title: "Olaris 0.4: Endless"
featuredImage: "posts/olaris-0.4.0/endless.jpg"
date: 2022-04-02T12:52:25+01:00
description: "Olaris 0.4.0 is Olaris‘ fourth feature release. This release includes support for Postgres, MySQL and other external databases. Android and Windows applications and much more!"
categories: ["Release", "Updates"]
draft: false
---

It's been a while since our last release but we are still around and kicking! After the last release a lot of people found their way over to our little development community and contributed fixes and features and it's long overdue that these features find their way into a stable release. This is the most community driven Olaris release till date and I want to extend a big "thank you" to everybody that contributed to this release!

This release is named "Endless" after the Benson and Moorhead movie [The Endless](https://www.themoviedb.org/movie/430231-the-endless), if you haven't seen it yet check it out!

![Screenshot of the new Overview](overview.png)

### Database support

The first feature I want to talk about is external database support. By default Olaris uses a on-disk SQLite database that works fine for most libraries. For people with larger collections though having a full-fledged database server might increase performance and reliability. 

When starting olaris-server simply provide a `--db-conn` flag with a connectionstring for your database. For instance `olaris-server serve --db-conn", "mysql://olaris:password@localhost/olaris-database?charset=utf8&parseTime=True&loc=Local`. If you just want to play around with the feature you can use docker-compose from the olaris-server repository and just issue a `docker-compose up` to start an olaris-server and mysql container.

### Android & Windows applications

We also have our first native apps for Olaris (not counting Chromecast). 

![Windows app screenshot](windows.jpeg)
Chris Brower released his Windows (UWP) application on the Microsoft store; You can [download it here](https://www.microsoft.com/en-us/p/olaris/9npgfbf1nd3q#activetab=pivot:overviewtab) and find the code on [Gitlab](https://gitlab.com/olaris/olaris-windows).  

![Android app screenshot](android.png)

Animazing published the first version of the Android app for phones and tablets. You can download it on the [Google Play Store](https://play.google.com/store/apps/details?id=tv.olaris.android) or build it yourself from our [Gitlab repository](https://gitlab.com/olaris/olaris-android)

### The rest

![Unidentified screenshot](unidentified.png)

There are tons of smaller and experimental features and fixes added here are just a few:

* Initial support for reading xattr to lookup metadata directly from the filesystem;
* Match unidentified movies by manually tagging them;
* Auto-suggest pathing information when adding libraries;
* Support for external Rclone configurations;
* Direct content tagging via an TMDB ID;
* Library sorting;

And much more!

### Getting started

The easiest way to try out Olaris is to [use our pre-build Docker image](https://hub.docker.com/r/olaristv/olaris-server). If you're on Linux, you can [download our prebuilt binary](https://storage.googleapis.com/bysh-chef-files/olaris-release/olaris-linux-amd64-v0.4.0.zip) or just [run from source directly](https://gitlab.com/olaris/olaris-server). For more documentation on how to run Olaris, see our [README](https://gitlab.com/olaris/olaris-server/blob/develop/README.md).

If you are using an Appbox from [Bytesized hosting](http://bytesized-hosting.com/) you can install Olaris from the App center or, if it's already installed, restart for the new version to boot up.

We hope that you enjoy these new features. As always, please let us know about your experience. Hit us up on [Discord](https://discord.gg/eKHsJV3), report bugs on our [bugtracker](https://gitlab.com/olaris/olaris-server/issues) or find us on [Reddit](https://www.reddit.com/r/olaris/).

### Your help is needed!

Olaris is very much a community project and building a media server is no small feat. We are always looking for Go developers for the backend, React developers for the frontend and application developers for any other platforms. We don't even have a proper website yet so all the help is very much welcome!
