---
title: "Olaris 0.1: Huygens Hurray"
featuredImage: "https://i.imgur.com/djgxhhN.jpg"
date: 2019-05-14T10:00:53+02:00
description: "Olaris is an open-source, community-driven, media server for your movies and TV shows. Over the last few months myself (Animazing) and two freelancers (Ben on the front-end and Leon on the transcoding server) have been working on Olaris whenever time and money allowed it. We are now ready to announce it via this first public Alpha release."
categories: ["Release", "Updates"]
draft: false
---
There are a couple of reasons why we feel we need yet an other Media Server.

### Neither Plex nor Emby were designed with hosting from a datacenter in mind.
Emby and Plex were designed to be hosted from inside your house, every design decision was taken with this in mind. Which means there are a lot of extra features and bulk within the application that hinders your experience as a user. It also means we often need to do weird workarounds just to keep Plex from playing nice with our servers.

## Plex is way too centralised and dependent on Plex's infrastructure.
If the Plex login servers go down, everything is down. Plex's authentication service would be great if it was optional but sadly it is not. It's not only authentication, if for some reason Plex thinks your Plex port is not open to the public because an external check on their server fails it will setup a relay server and push all traffic through their own servers.

### Plex is adding features that we don't want.
I can't speak for all of you but personally the direction Plex has been taken lately has not resulted in features I wanted to see. Everytime I login now I am being forced fed "Podcasts" and "Web Shows" that I did not ask for. I am a paid user so I think I should be able to turn these features off at the very least.

It all comes down to control; we want a product we can control so we can build the features the community really wants.

### What Olaris is today
We are ofcourse not kidding ourselves; building a streaming server is very hard and on top of that we are competing with Plex which has had investor money and over 10 years of experience. So we are going to take this one step at a time. Let me tell you what we have so far.

#### Metadata support for Movie and TV Show libraries
We support movies and season/episode based series as long as they follow the default "Movie (Year)" or "Series S01E01" format. Anime and datebased series are not officially supported yet some work some dont. Olaris tries to one thing, and do it well: browse and consume your Movies and TV Shows. This is why we do not intend to support audio content.

#### Transmuxing
We support Direct-Play/Direct-Stream like playback via our built-in browser player. Our software will always try to deliver the content in the best quality and only fallback on transcoding when it's absolutely not possible to play the original file.

#### Transcoding
We support on the fly transcoding and switching between various transcoding profiles. This is useful if a file's coded is not supported by your playback device or your connection does not allow it to play in it's original bitrate.

#### Subtitles
We support built-in and external subtitles, even when transmuxing so no forced transcoding needed for subtitles.

#### User management
There is one default admin user who can invite multiple standard users to their server using invite codes. It's also completely decentralised so no logging in through a third-party server.

Give Olaris a spin and let us know what you think.
